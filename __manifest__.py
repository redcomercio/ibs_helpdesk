# -*- coding: utf-8 -*-
# © IBS North Africa. See LICENSE file for full copyright & licensing details.

{
    "name"       : "IBS Helpdesk",
    "version"    : "2.6.0",
    "category"   : "Discuss",
    "author"     : "odoo.ma (IBS Group)",
    "summary"    : "Your mailbox is full? you have difficulties following and controlling your customer communication? Then this module is for you.",
    "website"    : "www.odoo.ma",
    "license"    : "LGPL-3",
    "installable": True,
    "active"     : False,
    "application": True,

    "depends": [ 
        "base",
        "mail",
        "fetchmail",
        "board"
    ],

    "data": [
        "security/ibs_helpdesk.xml",
        "security/ir.model.access.csv",

        "views/ibs_ticket.xml",
        "views/ibs_ticket_deadline.xml",
        "views/ibs_ticket_category.xml",
        "views/ibs_ticket_priority.xml",
        "views/ibs_ticket_response.xml",
        "views/ibs_ticket_source.xml",
        "views/res_users.xml",
        "views/res_company.xml",

        "workflow/ibs_ticket.xml",

        "wizard/views/ibs_ticket_open.xml",

        "views/menus.xml",

        "views/assets.xml"
    ]
}