# -*- coding: utf-8 -*-
# Copyright 2013-2016 IBS North Africa
# This is the property of IBS North Africa, any reproduction or modification without
# given consent is strictly prohibited and can be prosecuted to the full extent of the law.
# www.odoo.ma / contact@odoo.ma

{
    "name": "IBS Helpdesk 10",
    "version": "2.5.1",
    "category": "Discuss",
    "author": "odoo.ma (IBS Group)",
    "summary": "Your mailbox is full? you have difficulties following and controlling your customer communication? Then this module is for you.",
    "website": "www.odoo.ma",
    "author": "odoo.ma (IBS Group)",
    "license": "LGPL-3",

    "depends": [ "base", "mail", "fetchmail", "board" ],

    "data": [
        "security/ibs_helpdesk.xml",
        "security/ir.model.access.csv",

        "views/ibs_ticket.xml",
        "views/ibs_ticket_deadline.xml",
        "views/ibs_ticket_category.xml",
        "views/ibs_ticket_priority.xml",
        "views/ibs_ticket_response.xml",
        "views/ibs_ticket_source.xml",

        "views/res_users.xml",
        "views/res_company.xml",

        "workflow/ibs_ticket.xml",

        "wizard/views/ibs_ticket_open.xml",

        "views/menus.xml",

        "views/assets.xml"
    ],
    "installable": True,
    "active": False,
    "application": True
}