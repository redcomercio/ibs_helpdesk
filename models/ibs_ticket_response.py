# -*- coding: utf-8 -*-
# © IBS North Africa. See LICENSE file for full copyright & licensing details.

from datetime           import datetime
from openerp            import api, fields, models, _
from openerp            import netsvc, SUPERUSER_ID
from openerp.exceptions import Warning
from openerp.tools      import DEFAULT_SERVER_DATETIME_FORMAT

import base64
import logging
_logger = logging.getLogger(__name__)

class ibs_ticket_response( models.Model ):
    _name  = "ibs.ticket.response"
    _order = "id desc"

    @api.one
    @api.depends( "ticket_id"  )
    def _get_company_id( self ):
        for response in self:
            response.company_id = response.ticket_id.company_id    

    body_html = fields.Html(
        string    = "Message",
        required  = True,
        translate = True
    )

    date = fields.Datetime( 
        string    = "Date",
        readonly  = True,
        translate = True
    )

    ticket_id = fields.Many2one(
        comodel_name = "ibs.ticket",
        string       = "Ticket",
        invisible    = True,
        translate    = True
    )

    agent = fields.Many2one(
        comodel_name = "res.users",
        string       = "Agent",
        readonly     = True,
        translate    = True
    )

    company_id = fields.Many2one(
        compute      = "_get_company_id",
        comodel_name = "res.company",
        string       = "Company",
        store        = True,
        translate    = True
    )

    # company_id = fields.Many2one(
    #     related   = "company_id.id",
    #     string    = "Company",
    #     store     = True ,
    #     translate = True
    # )

    state = fields.Selection(
        related   = "ticket_id.state",
        string    = "State",
        store     = True,
        translate = True
    )

    lang = fields.Char(
        string    = "Language",
        translate = True
    )

    @api.model
    def default_get( self, fields ):
        user_obj  = self.env[ "res.users" ]
        user      = user_obj.browse( [ self.env.user.id ] )
        ticket_id = ( 
                        "ticket_id" in self.env.context 
                    ) and self._context[ "ticket_id" ] or None

        defaults  = {
            "date"     : datetime.now().strftime( DEFAULT_SERVER_DATETIME_FORMAT ),
            "agent"    : self.env.user.id,
            "ticket_id": ticket_id,
            "lang"     : user.partner_id.lang
        }

        if "default_body_html" in self.env.context:
            defaults[ "body_html" ] = self._context[ "default_body_html" ]

        return defaults

    @api.multi
    def send( self, email_from, email_to, subject, body, mail_server_id, reply_to = None,
              attachments = None, email_bcc = None, email_cc = None ):
        ir_mail_server = self.env[ "ir.mail_server" ]
        
        msg = ir_mail_server.build_email(
            email_from          = email_from,
            email_to            = email_to,
            reply_to            = reply_to,
            subject             = subject,
            body                = body,
            email_bcc           = email_bcc,
            email_cc            = email_cc,
            attachments         = attachments,
            subtype             = "html",
            subtype_alternative = "plain",
            object_id           = None,
            # This is for custom mail servers such as mailgun.
            headers             = { "h:Reply-To": reply_to }
        )

        res = ir_mail_server.send_email(
            msg,
            mail_server_id = mail_server_id
        )
        
        if not res:
            raise Warning( _( "Please setup your helpdesk." ) )

    @api.multi
    def _check_email_validity( self, values ):
        fetchmail_server_obj = self.env[ "fetchmail.server" ]
        found                = False

        if "email" in values:
            found = fetchmail_server_obj.sudo().search_count( [ 
                        ( "user", "=", values[ "email" ] ) 
                    ] )

        if found:
            raise Warning( _(
                "This email address is associated with an incoming email server."
            ) )

    @api.multi
    def _update_ticket( self, response, send_email, update_count = True ):
        ticket_obj = self.env[ "ibs.ticket" ]
        ticket     = response.ticket_id

        # Update the ticket count.
        to_write = {}

        if update_count:
            to_write[ "responses_count" ] = ticket.responses_count + 1
        
        # When the user is the first to respond and the agent isn't set yet,
        # make him the ticket's agent.
        if send_email and not ticket.agent:
            to_write[ "agent" ] = self.env.uid

        if to_write:
            ticket_rec = ticket_obj.browse( [ ticket.id ] )

            ticket_rec.responses_count = to_write[ "responses_count" ]

            if "agent" in to_write:
                ticket_rec.agent = to_write[ "agent" ]

        return ticket

    # Check if we have configured an outgoing email server.
    @api.multi
    def _check_outgoing_mail_server( self, data, mail_server_id = False ):
        user_obj        = self.env[ "res.users" ]
        mail_server_obj = self.env[ "ir.mail_server" ]
        user            = user_obj.browse( [ self.env.user.id ] )

        # We didn't get another mail server, fallback to default.
        if not mail_server_id:
            mail_server_id = user.company_id.default_mail_server \
            and user.company_id.default_mail_server.id or None

        if not mail_server_id:
            raise Warning( _( "Please setup an outgoing email server." ) )

        return mail_server_obj.sudo().browse( [ mail_server_id ] )

    # Prepare the FROM field of the email we are sending.
    def _prepare_from_email( self, data ):
        mail_server = data[ "mail_server" ]

        # We either use Reply To incase of mail services like mailgun
        # or the smpt_user in normal cases.

        return "%s <%s>" % ( 
            mail_server.name,
            mail_server.x_reply_to or mail_server.smtp_user
        )

    # Prepare the TO field of the email we are sending.
    def _prepare_to_email( self, data ):
        response = data[ "response" ]

        return response.ticket_id.email

    # Prepare the SUBJECT field of the email we are sending.
    def _prepare_subject( self, data ):
        # CHANGELOG: added data[ "response" ].ticket_id since data[ "ticket" ] 
        # ends to be empty when the ticket is already created and called from 
        # forward function in premium version .10
        ticket  = data[ "ticket" ] or data[ "response" ].ticket_id
        subject = _( "Ticket: " ) + ticket.name
        
        if ticket.subject:
            subject = _( "Ticket %s: %s " ) % ( ticket.name, ticket.subject )

        return subject

    # Since we have responded to the ticket, its state must reflect it.
    def _update_ticket_workflow( self, data ):
        ticket    = self.env[ "ibs.ticket" ]
        ticket_id = data[ "ticket" ].id

        ticket.browse( [ ticket_id ] ).signal_workflow( "answer_posted" )

    def _prepare_email( self, data ):
        return data

    @api.multi
    def _send_email( self, data ):
        self.send(
            email_from     = data[ "email_from" ], 
            email_to       = [ data[ "email_to" ] ],
            reply_to       = data[ "email_from" ],
            subject        = data[ "subject" ], 
            body           = data[ "response" ].body_html, 
            mail_server_id = data[ "mail_server" ].id
        )

    @api.model
    def create( self, values, send_email = True, send_auto_response = False ):
        data = {}

        # Save the response and hold its ID to be used within the email model.
        response_id        = super( ibs_ticket_response, self ).create( values )
        data[ "response" ] = response_id # self.browse( [ response_id ] )

        self._check_email_validity( values )

        data[ "ticket" ] = self._update_ticket( data[ "response" ], send_email )

        if send_email:
            data[ "mail_server" ] = self._check_outgoing_mail_server( data )            
            data[ "email_from" ]  = self._prepare_from_email( data )
            data[ "email_to" ]    = self._prepare_to_email( data )
            data[ "subject" ]     = self._prepare_subject( data )
            data                  = self._prepare_email( data )

            self._send_email( data )
            self._update_ticket_workflow( data )

        if send_auto_response:
            self.send_customer_automatic_response( response_id )

        return response_id

    def save( self ):
        return True

    def send_customer_automatic_response( self, response_id, automatic_response = False ):
        user_obj     = self.env[ "res.users" ]
        category_obj = self.env[ "ibs.ticket.category" ]
        template_obj = self.env[ "mail.template" ]
        mail_obj     = self.env[ "mail.mail" ]
        user         = user_obj.browse( [ self.env.user.id ] )

        if not automatic_response:
            automatic_response = user.company_id.automatic_response and \
                                 user.company_id.automatic_response.id or None

        if automatic_response:
            template_id = template_obj.browse( 
                [ automatic_response ]
            )

            mail_id = template_id.send_mail(
                response_id.id,
                force_send = True
            )

            mail_state = mail_obj.browse( [ mail_id ] ).read(
                [ "state" ]
            )

            if mail_state and mail_state[ 0 ][ "state" ] == "exception":
                raise Warning( _( "Please setup your helpdesk." ) )
