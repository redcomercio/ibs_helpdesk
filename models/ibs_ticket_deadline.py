# -*- coding: utf-8 -*-
# © IBS North Africa. See LICENSE file for full copyright & licensing details.

from datetime import datetime
from openerp  import api, fields, models, _
                    
class ibs_ticket_deadline( models.Model ):
    _name= "ibs.ticket.deadline"
    
    name = fields.Char( 
        string    = "Name",
        size      = 10,
        required  = True,
        translate = True 
    )

    value_day = fields.Integer( 
        string    = "Days",
        translate = True
    )

    value_hours = fields.Integer(
        string    = "Hours",
        translate = True
    )

