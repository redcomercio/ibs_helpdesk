# -*- coding: utf-8 -*-
# © IBS North Africa. See LICENSE file for full copyright & licensing details.

from openerp import api, fields, models, _

class res_company( models.Model ):
    _inherit = "res.company"

    default_mail_server = fields.Many2one( 
        comodel_name = "ir.mail_server",
        string       = "Outgoing email server",
        ondelete     = "restrict",
        translate    = True
    )

    automatic_response = fields.Many2one(
        comodel_name = "mail.template",
        string       = "Automatic response",
        ondelete     = "restrict",
        translate    = True
    )


class ir_mail_server( models.Model ):
    _inherit = "ir.mail_server"

    x_reply_to = fields.Char( 
        string    = "Reply to",
        translate = True
    )

