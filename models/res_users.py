# -*- coding: utf-8 -*-
# © IBS North Africa. See LICENSE file for full copyright & licensing details.

from openerp            import api, fields, models, _
from openerp.exceptions import  Warning

class res_users( models.Model ):
    _inherit = "res.users"

    def _open_tickets( self ):
        ticket_obj = self.env[ "ibs.ticket" ]

        for line in self:
            open_tickets = ticket_obj.search( [ 
                ( "agent", "=", line.id ),
                ( "state", "not in", [ "draft", "resolved", "cancel" ] )
            ] )
            line.open_tickets_number = len( open_tickets )

    def _remaining_tickets_number( self ):
        for user in self:
            open_tickets = user.open_tickets_number
            max_tickets  = user.max_tickets

            if max_tickets > open_tickets :
                user.remaining_tickets_number = max_tickets - open_tickets
            else :
                user.remaining_tickets_number = 0

    def check_ticket_capacity( self, user_id ):
        remaining_tickets_number = self.browse( [ user_id ] ).remaining_tickets_number

        if remaining_tickets_number <= 0:
            raise Warning( _( "This agent has reached his maximum ticket capacity." ) )

        return True

    max_tickets = fields.Integer( 
        string    = "Maximum tickets",
        default   = 1,
        translate = True
    )

    open_tickets_number = fields.Integer( 
        compute   = "_open_tickets",
        string    = "Open tickets",
        translate = True
    )

    remaining_tickets_number = fields.Integer(
        compute   = "_remaining_tickets_number",
        string    = "Remaining tickets",
        readonly  = True,
        translate = True
    )

    response_signature = fields.Html( 
        string    = "Signature",
        translate = True
    )

    category_ids = fields.Many2many( 
        comodel_name = "ibs.ticket.category",
        relation     = "agent_category_rel",
        column1      = "helpdesk_agent_id",
        column2      = "ticket_category_id",
        string       = "Categories",
        translate    = True
    )

    _sql_constraints = [
        ( "ibs_helpdesk_max_tickets_constraint",
          "CHECK ( max_tickets > 0 )",
          "Maximum tickets must be positive." ),
    ]
