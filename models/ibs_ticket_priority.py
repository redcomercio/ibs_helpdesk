# -*- coding: utf-8 -*-
# © IBS North Africa. See LICENSE file for full copyright & licensing details.

from openerp import api, fields, models, _
             
class ibs_ticket_priority( models.Model ):
    _name = "ibs.ticket.priority"

    name = fields.Char( 
        string    = "Name",
        size      = 50,
        required  = True,
        translate = True
    )
