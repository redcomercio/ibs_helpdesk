# -*- coding: utf-8 -*-
# © IBS North Africa. See LICENSE file for full copyright & licensing details.

from datetime           import datetime, timedelta
from email.utils        import getaddresses
from openerp            import netsvc, SUPERUSER_ID, api, fields, models, _
from openerp.exceptions import Warning
from openerp.tools      import html_sanitize, DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.mail import email_split

import logging
import pytz
import re

module_name = "ibs_helpdesk"

_logger = logging.getLogger(__name__)

def ticket_email_split( raw_email ):
    """ Return a list of the email addresses found in ``text`` """
    if not raw_email:
        return []

    addresses = getaddresses( [ raw_email ] )
    email     = addresses[ 0 ]

    if email[ 1 ] and "@" in email[ 1 ]:
        return email

    return []

def _interpolate( s, d ):
    if s:
        return s % d
    return ""

def _interpolation_dict( context ):
    now = range_date = effective_date = datetime.now( 
                                            pytz.timezone(
                                                    context.get( "tz" ) or "UTC" 
                                            )
                                        )

    if context.get( "ir_sequence_date" ):
        effective_date = datetime.strptime( 
                                context.get( "ir_sequence_date" ), "%Y-%m-%d"
                        )
    if context.get( "ir_sequence_date_range"):
        range_date = datetime.strptime(
                                context.get( "ir_sequence_date_range" ), "%Y-%m-%d"
                        )

    sequences = {
        "year": "%Y", "month": "%m", "day": "%d", "y": "%y", "doy": "%j", "woy": "%W",
        "weekday": "%w", "h24": "%H", "h12": "%I", "min": "%M", "sec": "%S"
    }
    res = {}
    for key, sequence in sequences.iteritems():
        res[ key ]              = effective_date.strftime( sequence )
        res[ "range_" + key ]   = range_date.strftime( sequence )
        res[ "current_" + key ] = now.strftime( sequence )

    return res

IBS_TICKET_STATES = [
    ( "draft"            , "Draft" ),
    ( "opened"           , "Open" ),
    ( "awaiting_response", "Awaiting response" ),
    ( "answered"         , "Responded" ),
    ( "resolved"         , "Resolved" ),
    ( "cancel"           , "Canceled" )
]

class ibs_ticket( models.Model ):
    _name        = "ibs.ticket"
    _order       = "id DESC"
    _description = "Tickets"
    _inherit     = [ "mail.thread", "ir.needaction_mixin" ]
    _order       = "date desc"

    @api.one
    @api.depends( "category" )
    def _get_deadline( self ):
        deadlines    = {} 
        category_obj = self.env[ "ibs.ticket.category" ]

        for line in self:
            # Only calculate deadline if category is present.
            if line.category:
                category    = category_obj.browse( [ line.category.id ] )
                value_day   = category.deadline.value_day
                value_hours = category.deadline.value_hours

                deadline = datetime.strptime( line.date, DEFAULT_SERVER_DATETIME_FORMAT ) \
                + timedelta( days = value_day ) + timedelta( hours = value_hours ) 
                
                line.deadline = deadline.strftime( DEFAULT_SERVER_DATETIME_FORMAT )
            else:
                line.deadline = line.date

    # HACK: due to recursive problem whnen using write method we use instead the sql query
    @api.model
    def _set_deadline( self ):
        self.env.cr.execute( "UPDATE ibs_ticket SET deadline = %s WHERE id = %s",
                    ( self.deadline, self.id, ) )

        return True

    @api.one
    @api.depends( "deadline", "date"  )
    def _age_ticket( self ):
        ages = {}        
        now  = datetime.now()

        for line in self:
            age     = ""
            hours   = 0
            minutes = 0

            if line.date:
                diff = now - datetime.strptime( line.date, DEFAULT_SERVER_DATETIME_FORMAT )

                if diff.seconds >= 3600:
                    hours = ( diff.seconds / 60 ) / 60
                else:
                    minutes = diff.seconds / 60

                if diff.days <= 1:
                    age = _( "%s h %s m" ) % ( hours, minutes )
                else:
                    age = _( "%s d %s h" ) % ( diff.days, hours )

                line.age = age

    def run_scheduler( self ):
        _logger.info( _( "Late tickets scheduler is running..." ) )
        
        tickets = self.search( [ 
                    ( "state", "in", [ "opened", "awaiting_response" ] ) 
                ] )
        
        today = datetime.now().strftime( DEFAULT_SERVER_DATETIME_FORMAT )

        for line in self.browse( tickets ):
            if ( line.deadline < today ):
                late_ticket_template = line.agent.company_id.late_ticket_template

                if late_ticket_template:
                    template_obj = self.env[ "mail.template" ]
                    mail_obj     = self.env[ "mail.mail" ]
                    mail_id      = template_obj.send_mail( 
                                    late_ticket_template.id,
                                    line.id,
                                    force_send = True
                                )
                else:
                    _logger.warning( _( "Late tickets cron: Please configure a \
                        template in your company helpdesk settings." ) )
        
        return True

    @api.one
    @api.depends( "deadline" )
    def _is_late( self ):
        for ticket in self:
            if not ticket.deadline:
                ticket.is_late = False
                continue

            deadline = datetime.strptime( ticket.deadline, DEFAULT_SERVER_DATETIME_FORMAT )
            ticket.is_late = ( deadline < datetime.now() ) and \
            ( ticket.state in [ "opened", "awaiting_response" ] )

    name = fields.Char( 
        string    = "Ref.",
        size      = 20,
        required  = True,
        readonly  = True,
        select    = True,
        default   = "/",
        translate = True
    )

    state = fields.Selection(
        selection        = IBS_TICKET_STATES,
        string           = "State",
        readonly         = True,
        track_visibility = "onchange",
        default          = "draft",
        translate        = True
    )

    client = fields.Many2one(
        comodel_name = "res.partner",
        string       = "Client",
        readonly     = False,
        states       = {
            "resolved": [ ( "readonly", True ) ],
            "cancel"  : [ ( "readonly", True ) ]
        },
        ondelete     = "restrict",
        translate    = True
    )

    email = fields.Char(
        string    = "Email",
        size      = 255,
        translate = True
    )

    subject = fields.Char(
        string    = "Subject",
        size      = 255,
        readonly  = False,
        states    = {
            "resolved": [ ( "readonly", True ) ],
            "cancel"  : [ ( "readonly", True ) ]
        },
        translate = True
    )

    category = fields.Many2one(
        comodel_name = "ibs.ticket.category",
        string       = "Category",
        readonly     = False,
        states       = {
            "resolved": [ ( "readonly", True ) ],
            "cancel"  : [ ( "readonly", True ) ]
        },
        ondelete     = "restrict",
        translate    = True
    )

    source = fields.Many2one(
        comodel_name = "ibs.ticket.source",
        string       = "Source",
        readonly     = False,
        states       = {
            "resolved": [ ( "readonly", True ) ]
        },
        translate    = True
    )

    agent = fields.Many2one(
        comodel_name = "res.users",
        string       = "Agent",
        readonly     = True,
        translate    = True
    )

    priority = fields.Many2one(
        comodel_name = "ibs.ticket.priority",
        string       = "Priority",
        readonly     = False,
        states       = {
            "resolved": [ ( "readonly", True ) ]
        },
        translate    = True
    )

    date = fields.Datetime(
        string    = "Date",
        readonly  = True,
        states    = {
            "draft": [ ( "readonly", False ) ]
        },
        default   = lambda *a: datetime.now().strftime( 
                                                DEFAULT_SERVER_DATETIME_FORMAT
                                            ),
        translate = True
    )

    deadline = fields.Datetime( 
        compute   = "_get_deadline",
        inverse   = "_set_deadline",
        states    = {
            "cancel"  : [ ( "readonly", True ) ],
            "resolved": [ ( "readonly", True ) ]
        },
        string    = "Deadline",
        store     = True,
        translate = True
    )

    age = fields.Char(
        compute   = "_age_ticket",
        string    = "Age",
        store     = False,
        translate = True
    )

    resolution_date = fields.Datetime(
        string    = "Resolution",
        readonly  = True,
        states    = {
            "resolved": [ ( "readonly", False ) ]
        },
        translate = True
    )

    responses = fields.One2many(
        comodel_name = "ibs.ticket.response",
        inverse_name = "ticket_id",
        string       = "Responses",
        translate    = True
    )

    responses_count = fields.Integer(
        string    = "Responses count",
        default   = 0,
        translate = True
    )

    company_id = fields.Many2one(
        comodel_name = "res.company",
        string       = "Company",
        required     = True,
        default      = lambda self = {}: self.env[ "res.users" ].browse( 
                                        [ self.env.user.id ] ).company_id.id,
        translate    = True
    )

    is_late = fields.Boolean(
        compute   = "_is_late",
        string    = "Is late",
        store     = False,
        translate = True
    )

    notes = fields.Html(
        string    = "Notes",
        translate = True
    )

    _sql_constraints = [
        ( "name_uniq" ,
          "UNIQUE( name, company_id )",
          "This reference already exists." ),
    ]

    @api.model
    def create( self, values ):
        values[ "name" ] = self.env[ "ir.sequence" ].next_by_code( 
                                                            "ibs.ticket.reference"
                                                        )

        return super( ibs_ticket, self ).create( values )

    @api.multi
    def open( self ):
        mod_obj = self.env[ "ir.model.data" ]
        act_obj = self.env[ "ir.actions.act_window" ]
        ticket  = self.browse( self.ids[ 0 ] )

        if ticket.state != "draft":
            raise Warning( _( "This ticket has already been opened." ) )

        result = mod_obj.get_object_reference( 
                                                module_name,
                                                "action_ibs_ticket_open_wizard"
                                            )

        id     = result and result[ 1 ] or False
        action = act_obj.browse( [ id ] )
        result = action.read()[ 0 ]
        result[ "target" ]     = "new"
        result[ "nodestroy"]   = True
        new_context = dict( self.env.context ).copy()       
        new_context.update( { "ticket_id": ticket.id } )

        result[ "context" ]    = new_context
        
        return result

    @api.multi
    def response( self ):
        mod_obj = self.env[ "ir.model.data" ]
        act_obj = self.env[ "ir.actions.act_window" ]
        ticket  = self.browse( self.id )

        if ticket.state in ( "draft", "resolved", "cancel" ):
            raise Warning( _( u"This ticket is not open." ) )

        result = mod_obj.get_object_reference( 
                                                module_name,
                                                "action_ibs_ticket_response"
                                            )

        id     = result and result[ 1 ] or False
        action = act_obj.browse( [ id ] )
        result = action.read()[ 0 ]
        result[ "target" ]     = "new"
        result[ "nodestroy"]   = True

        # Fill body with the signature and the previous response.
        first_message = ticket.responses and ticket.responses[ 0 ] \
        and ticket.responses[ 0 ].body_html or ""

        body = ""
        # In same cases we don't want to add the signature.
        if "no_signature" not in self.env.context:
            body = "<br/>%s" % ( ticket.agent.response_signature or "", )

        if first_message:
            body = body + "<br/><blockquote style=\"margin-top: 10px; \
                margin-bottom: 10px;margin-left: 0px;padding-left: 15px; \
                border-left: 3px solid #ccc;\">%s</blockquote>" % (  first_message )

        new_context = dict( self.env.context ).copy()       
        new_context.update( { 
                                "ticket_id": ticket.id,
                                "default_body_html": body
                            } )

        result[ "context" ] = new_context
        
        return result

    @api.multi
    def unlink( self, force = False ):
        for line in self:
            if not force and line.state not in ( "draft" ):
                raise Warning( _( "You cannot delete this ticket!" ) )

            super( ibs_ticket, line ).unlink()

    @api.one
    def wkf_resolve_ticket( self ):
        self.write( {
            "state"          : "resolved",
            "resolution_date": datetime.now().strftime( DEFAULT_SERVER_DATETIME_FORMAT )
        } )

        return True

    def _process_raw_mail( self, msg ):
        mail          = {}
        ticket_data   = {}
        response_data = {}

        mail[ "subject" ]        = msg.get( "subject" )
        ticket_data[ "subject" ] = mail[ "subject" ]

        # In order to avoid long non wrapping lines.
        # Changed html_email_clean to html_sanitize as the former is no more supported
        body = msg.get( "body" )
        response_data[ "body_html" ] = body and \
        html_sanitize( body ).replace( "&nbsp;", " " ).replace( "&#160;", " " ) \
        or ""

        # Hack: Replace comma since some people have them in their full names, which breaks our parser.
        email = ticket_email_split( msg.get( "from" ).replace( ",", "" ) )

        mail[ "from" ]           = email and email[ 1 ] or msg.get( "from" )
        ticket_data[ "email" ]   = mail[ "from" ]
        response_data[ "agent" ] = None

        mail[ "response_data" ]  = response_data
        mail[ "ticket_data" ]    = ticket_data
        mail[ "cc" ]             = msg.get( "cc" )

        return mail

    def _get_ticket_id( self, subject ):
        mod_obj   = self.env[ "ir.model.data" ]
        seq_obj   = self.env[ "ir.sequence" ]
        ticket_id = None

        sequence_results = mod_obj.get_object_reference( module_name, "sequence_ibs_ticket" )
        sequence_id      = sequence_results and sequence_results[ 1 ] or False

        if not sequence_id:
            raise Warning( _( "Ticket sequence is not defined." ) )

        sequence              = seq_obj.browse( [ sequence_id ] )
        subject_ticket_number = False

        if subject:
            d = _interpolation_dict( self.env.context )
            
            try:
                interpolated_prefix = _interpolate( sequence.prefix, d )
                interpolated_suffix = _interpolate( sequence.suffix, d )

                pattern = "%s[0-9]{%s}%s" % (
                    re.escape( interpolated_prefix ),
                    str( sequence.padding ),
                    re.escape( interpolated_suffix )
                )

                subject_ticket_number = re.search( pattern, subject )
            except ValueError:
                raise Warning( _( "Invalid ticket sequence." ) )

        if subject_ticket_number:
            ticket_results = self.search( [ 
                    ( "name", "=", subject_ticket_number.group() ) 
                ], limit = 1 )
            ticket_id = ticket_results and ticket_results[ 0 ] or False
        return ticket_id

    def _get_client( self, email_from ):
        partner_obj = self.env[ "res.partner" ]

        client = partner_obj.search( [ 
                ( "email", "=", email_from ) 
            ], limit = 1 )

        client = client.id and client[ 0 ].id or None

        return client

    def _process_new_ticket( self, ticket_data ):
        ticket_id = None
        try:
            ticket_id = self.create( ticket_data )
        except Exception as err:
            return _logger.info( err )

        return ticket_id

    def _process_existing_ticket( self, ticket_id ):
        self.browse( [ ticket_id.id ] ).signal_workflow( "response_received" )

        return False

    def _create_response( self, mail ):
        response_obj = self.env[ "ibs.ticket.response" ]
        
        response_id  = response_obj.create( 
            mail[ "response_data" ],
            send_email         = False,
            send_auto_response = mail[ "send_auto_response" ]
         )

        return response_id

    # Process incoming emails.
    def message_new( self, msg, custom_values = None ):
        """
            - Process incoming email
            - Find ticket based on the subject
                - Create a new ticket if not found
            - Save response
        """
        mail        = self._process_raw_mail( msg )
        ticket_data = mail[ "ticket_data" ]
        ticket_id   = None

        # We have already done the processing somewhere else before. 
        # For instance when processing email black list.
        if "ticket_id" not in self.env.context:
            ticket_id = self._get_ticket_id( ticket_data[ "subject" ] )
        else:
            ticket_id = self._context[ "ticket_id" ]

        # Depending on configuration, the automatic response 
        # might be sent once only when a new ticket is received.
        send_auto_response      = True
        ticket_data[ "client" ] = self._get_client( mail[ "from" ] )

        if not ticket_id:
            ticket_id = self._process_new_ticket( ticket_data )
        else:
            # This process decides if we will send an automatic response as well.
            send_auto_response = self._process_existing_ticket( ticket_id )

        mail[ "send_auto_response" ]           = send_auto_response
        mail[ "ticket_data" ]                  = ticket_data
        mail[ "response_data" ][ "ticket_id" ] = ticket_id.id

        self._create_response( mail )

    # This method saves incoming messages in mail.message ( look in mail_thread.py ), we stop saving them in order to avoid a bug where if the customer responds to his own message,
    # Odoo considers it a response to an existing message which prevents message_new() from being executed.
    @api.multi
    @api.returns('self', lambda value: value.id)
    def message_post( self, body='', subject=None, message_type='notification',
                     subtype=None, parent_id=False, attachments=None,
                     content_subtype='html', **kwargs ):

        if type in [ "notification", "comment" ]: # Incoming emails are not saved, but only processed by message_new()
            return self.message_post( body = body, subject = subject, type = type, subtype= subtype,
                                    parent_id= parent_id, attachments= attachments,
                                    content_subtype = content_subtype, **kwargs )

class ibs_ticket_source( models.Model ):
    _name = "ibs.ticket.source"

    name  = fields.Char( "Name" )
