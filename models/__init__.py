# -*- coding: utf-8 -*-
# © IBS North Africa. See LICENSE file for full copyright & licensing details.

module_name = "ibs_helpdesk"

import res_company
import res_users
import ibs_ticket_deadline
import ibs_ticket_priority
import ibs_ticket_category
import ibs_ticket
import ibs_ticket_response